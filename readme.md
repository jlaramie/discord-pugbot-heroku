## discord-pugbot-heroku

A wrapper for the discord-pugbot. Please see the documentation for discord-pugbot to get setup.

<a href="https://www.npmjs.com/package/discord-pugbot"><img src="https://img.shields.io/npm/v/discord-pugbot.svg?maxAge=3600" alt="NPM version" /></a>

Heroku will need 2 config vars setup and be run as a worker.
- BOTTOKEN
- CLIENTID if you want it to display invites
- MAPS

Anything present in the discord-pugbot/config.json5 can be set as an environment variable and will get pulled into the configuration on start.

Please note, the hosted Heroku app above is for development and testing purposes only and not to be used in production. We recommend deploying a copy of this project on your own server.

A production-ready hosted service is TBD.

Or deploy your own Heroku instance!

## License
GPL 2.0

## Heroku Setup
https://dashboard.heroku.com/
git push heroku master
heroku ps:scale web=0 worker=1