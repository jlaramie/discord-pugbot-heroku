var exec = require('child_process').exec;

var child = exec('node ./node_modules/discord-pugbot/lib/index.js --init', {
    cwd: './'
});

child.stdout.on('data', function(data) {
    console.log(data.toString());
});

child.stdout.on('error', function(error) {
    console.log(error.toString());
});
