console.log('update config');
// Modify the config before start since heroku is unstable in that regard
var fs = require('fs'),
    JSON5 = require('json5')
config = JSON5.parse(fs.readFileSync(`./config.json5`).toString()),
    configKeys = Object.keys(config);


// some keys are commented out but are still wanted
configKeys.push('maps');

configKeys.forEach(function(key) {
    var envKey = key.toUpperCase(),
        envVal = process.env[envKey];

    if (process.env.hasOwnProperty(envKey)) {
        if (!isNaN(Number(envVal))) {
            config[key] = process.env[envKey];
        } else {
            try {
                config[key] = JSON.parse(process.env[envKey]);
            } catch (e) {
                config[key] = process.env[envKey];
            }
        }
    }
});

console.log('writing config');
fs.writeFileSync("./config.json5", JSON5.stringify(config));
